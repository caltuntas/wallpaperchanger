﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WallpaperChanger.SmashingMagazine
{
    public class WallpaperImage
    {
        public string Url { get; set; }
        public string Resolution { get; set; }
        public bool WithCalendar { get; set; }
        public int Width
        {

            get
            {
                var width = Resolution.Split('x')[0];
                return Convert.ToInt16(width);
            }
        }

        public int Height
        {

            get
            {
                var height = Resolution.Split('x')[1];
                return Convert.ToInt16(height);
            }
        }
    }
}
