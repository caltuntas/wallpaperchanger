﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WallpaperChanger.SmashingMagazine
{
    public class Wallpaper
    {        
        private List<WallpaperImage> images = new List<WallpaperImage>();

        public string Name { get; set; }
        public string PreviewImageUrl { get; set; }


        public void AddImage(WallpaperImage image)
        {
            images.Add(image);
        }

        public IEnumerable<WallpaperImage> Images
        {
            get
            {
                return images.AsReadOnly();
            }
        }

        public WallpaperImage FindForResolution(int width, int height,bool withCalendar)
        {
            Dictionary<double, WallpaperImage> imageRatios = new Dictionary<double, WallpaperImage>();
            foreach (var image in images.Where(x=>x.WithCalendar==withCalendar))
            {
                double widthRatio = (double)image.Width / (double)width;
                double heightRatio = (double)image.Height / (double)height;
                var key =Math.Abs(widthRatio - heightRatio);
                if(imageRatios.ContainsKey(key)==false)
                    imageRatios.Add(key, image);
            }

            var keyList = imageRatios.Keys.ToList();
            keyList.Sort();

            return imageRatios[keyList[0]];
        }
    }
}
