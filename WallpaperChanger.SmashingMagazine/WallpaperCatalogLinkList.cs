﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WallpaperChanger.SmashingMagazine
{
    public class WallpaperCatalogLinkList
    {
        private List<WallpaperCatalogLink> links = new List<WallpaperCatalogLink>();

        public List<WallpaperCatalogLink> Links
        {
            get
            {
                return links;
            }
        }

        public WallpaperCatalogLink GetLink(DateTime date)
        {
            foreach (var link in Links)
            {
                if (link.Date.Month == date.Month)
                    return link;
            }

            return null;
        }
    }
}
