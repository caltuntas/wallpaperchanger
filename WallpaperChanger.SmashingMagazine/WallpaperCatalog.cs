﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WallpaperChanger.SmashingMagazine
{
    public class WallpaperCatalog
    {
        private List<Wallpaper> wallpapers = new List<Wallpaper>();       
        public DateTime Date { get; set; }

        public List<Wallpaper> Wallpapers
        {
            get
            {
                return wallpapers;
            }
        }

        public Wallpaper GetWallpaper(string imageKey)
        {
            return Wallpapers.Where(x => x.Name == imageKey).FirstOrDefault();
        }
    }
}
