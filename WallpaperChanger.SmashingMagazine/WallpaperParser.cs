﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace WallpaperChanger.SmashingMagazine
{
    public class WallpaperParser
    {
        private const string BASE_URL = "http://www.smashingmagazine.com/";
        public List<Wallpaper> GetWallpapers(string URL)
        {
            List<Wallpaper> list = new List<Wallpaper>();
            var html = GetPage(URL);

            HtmlDocument htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(html);

            var mainWallpapers = htmlDoc.DocumentNode.Descendants("a").Where(a =>
            {
                 var val=a.GetAttributeValue("href", null);                                      
                 if(val!=null && (val.EndsWith(".jpg") || val.EndsWith(".png")) && a.InnerText=="preview")
                     return true;
                return false;
            }).Select(e=>e.GetAttributeValue("href",null));

   
            var urls = htmlDoc.DocumentNode.Descendants("a")
                                .Select(e => e.GetAttributeValue("href", null))
                                .Where(s => !String.IsNullOrEmpty(s) && (s.EndsWith(".jpg") || s.EndsWith(".png")));

            foreach (var previewUrl in mainWallpapers)
            {
                var name = previewUrl.Substring(previewUrl.LastIndexOf("/")+1).Replace("-preview","").Replace("-full","").Replace(".jpg","").Replace(".png","");
                Wallpaper wp = new Wallpaper();
                wp.Name = name;
                wp.PreviewImageUrl = previewUrl;

                var images = GetImages(urls, name);

                foreach (var image in images)
                {
                    wp.AddImage(image);
                }

                list.Add(wp);

            }
                                

            return list;
        }

        public WallpaperCatalogLinkList GetWallpaperCatalogLinks()
        {
            //http://www.smashingmagazine.com/2015/03/desktop-wallpaper-calendars-april-2015/
            WallpaperCatalogLinkList list = new WallpaperCatalogLinkList();
            var pattern = @"/(\d{4})/(\d{2})/";

            var url = "http://www.smashingmagazine.com/tag/wallpapers/";
            var html=GetPage(url);

            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(html);

            var links= doc.DocumentNode.Descendants("a").Select(e => e.GetAttributeValue("href", null));
            var urls = links.Where(s => !string.IsNullOrEmpty(s) && s.Contains("desktop-wallpaper-calendars") && s.Contains("#comments")==false).Distinct();

            foreach (var link in urls)
            {
                var match = Regex.Match(link, pattern);
                if (match.Success)
                {
                    int year = Convert.ToInt16(match.Groups[1].Value);
                    int month = Convert.ToInt16(match.Groups[2].Value);
                    var date = new DateTime(year, month,1);

                    var catalog = new WallpaperCatalogLink();                    
                    catalog.Date = date.AddMonths(1);
                    catalog.Link = link;
                    list.Links.Add(catalog);
                }
            }
            return list;
        }

        private List<WallpaperImage> GetImages(IEnumerable<string> allUrls, string name)
        {
            var pattern = name + @".*-(\d{1,4}x\d{1,4})\.(jpg|png)";
            List<WallpaperImage> list = new List<WallpaperImage>();
            foreach (var url in allUrls)
            {
                var match=Regex.Match(url, pattern);
                if (match.Success)
                {
                    list.Add(new WallpaperImage() { Url=url,Resolution=match.Groups[1].Value,WithCalendar=url.Contains("calendar")});
                }
            }
            return list;
        }

        private static string GetPage(string getUrl)
        {
            HttpWebRequest getRequest = (HttpWebRequest)WebRequest.Create(getUrl);
            getRequest.Timeout = 1 * 60 * 60 * 60;
            getRequest.Method = WebRequestMethods.Http.Get;
            getRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2";
            getRequest.AllowWriteStreamBuffering = true;
            getRequest.ProtocolVersion = HttpVersion.Version11;
            getRequest.AllowAutoRedirect = true;
            getRequest.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            getRequest.ContentType = "application/x-www-form-urlencoded";

           
            HttpWebResponse getResponse = (HttpWebResponse)getRequest.GetResponse();
            using (StreamReader sr = new StreamReader(getResponse.GetResponseStream()))
            {
                string sourceCode = sr.ReadToEnd();
                return sourceCode;
            }
        }

        public WallpaperCatalog GetWallpaperCatalog(WallpaperCatalogLink link)
        {
            var catalog = new WallpaperCatalog();
            catalog.Date = link.Date;
            var wallpapers=GetWallpapers(link.Link);
            catalog.Wallpapers.AddRange(wallpapers);
            return catalog;
        }
    }
}
