﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quartz;
using Quartz.Impl;

namespace WallpaperChanger.GUI
{
    public class WallpaperChangeScheduler
    {
        private ISchedulerFactory factory;

        public WallpaperChangeScheduler(ISchedulerFactory sf)
        {
            factory = sf;
        }

        public void Schedule()
        {            
            IScheduler sched = factory.GetScheduler();

            //IJobDetail job = JobBuilder.Create<WallpaperChangeJob>()
            //           .WithIdentity("job1", "group1")
            //           .Build();
            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("job1", "group1")
                .StartAt(DateBuilder.EvenMinuteDate(DateTime.UtcNow))
                .WithSimpleSchedule(x => x.WithIntervalInSeconds(30).RepeatForever())
                .Build();


            sched.ScheduleJob(trigger);

            sched.Start();
        }
    }
}
