﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyMessenger;

namespace WallpaperChanger.GUI
{
    public class SettingsChangedMessage:ITinyMessage
    {
        public string Interval { get; set; }

        public bool Startup { get; set; }

        public object Sender
        {
            get;
            private set;
        }
    }
}
