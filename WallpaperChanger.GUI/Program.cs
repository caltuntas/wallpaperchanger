﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Quartz.Impl;
using Quartz;
using Quartz.Spi;
using Ninject;
using WallpaperChanger.SmashingMagazine;
using TinyMessenger;
using Microsoft.Win32;

namespace WallpaperChanger.GUI
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {           
            var kernel = new StandardKernel();
            kernel.Bind<IJobFactory>().To<SimpleInjectorJobFactory>();
            kernel.Bind<Func<Type, IJob>>().ToMethod(ctx => t => (IJob)ctx.Kernel.Get(t));
            kernel.Bind<ISchedulerFactory>().To<StdSchedulerFactory>();
            kernel.Bind<WallpaperSettingsRepository>().ToSelf();
            kernel.Bind<ITinyMessengerHub>().To<TinyMessengerHub>();
            kernel.Bind<JobManager>().ToSelf().InSingletonScope();

            var triggerKey = new TriggerKey("wallpaperTrigger");
         
            var jobManager=kernel.Get<JobManager>();
            var messageHub= kernel.Get<ITinyMessengerHub>();

            ITrigger trigger = TriggerBuilder.Create()
               .WithIdentity(triggerKey)
               .StartAt(DateBuilder.EvenMinuteDate(DateTime.UtcNow))
               .WithSimpleSchedule(x => x.WithIntervalInMinutes(60).RepeatForever())
               .Build();
         
            IScheduler scheduler = jobManager.Add<WallpaperChangeJob>(trigger);
                
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            CustomApplicationContext context = new CustomApplicationContext(messageHub);

            messageHub.Subscribe<SettingsChangedMessage>((m) => 
            {
                var minutes = Convert.ToInt32(m.Interval);
                ITrigger newTrigger = TriggerBuilder.Create()
                   .WithIdentity(triggerKey)
                   .StartAt(DateBuilder.EvenMinuteDate(DateTime.UtcNow))
                   .WithSimpleSchedule(x => x.WithIntervalInMinutes(minutes).RepeatForever())
                   .Build();

                scheduler.RescheduleJob(triggerKey, newTrigger);

                RegistryKey key = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

                if (m.Startup == true && key.GetValue("WallpaperChanger") == null)
                {
                    key.SetValue("WallpaperChanger", "\"" + Application.ExecutablePath.ToString() + "\"");
                }
                else if(m.Startup==false && key.GetValue("WallpaperChanger") != null)
                {
                    key.DeleteValue("WallpaperChanger");
                }
            });

            Application.Run(context);            
        }
    }
}
