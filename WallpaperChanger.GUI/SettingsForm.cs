﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;
using TinyMessenger;

namespace WallpaperChanger.GUI
{
    public partial class SettingsForm : Form
    {
        private ITinyMessengerHub messageHub;
        public SettingsForm()
        {
            InitializeComponent();
            cbStartup.Checked = Startup;
        }

        public SettingsForm(ITinyMessengerHub messageHub):this()
        {            
            this.messageHub = messageHub;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            messageHub.Publish(new SettingsChangedMessage() {Interval= SelectedInterval,Startup=cbStartup.Checked});
        }

        private bool Startup
        {
            get
            {
                RegistryKey Key = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Run\");
                return Key.GetValue("WallpaperChanger") != null;
            }
        }

        private string SelectedInterval
        {
            get
            {
                foreach (Control control in this.gpInterval.Controls)
                {
                    if (control is RadioButton)
                    {
                        RadioButton radio = control as RadioButton;
                        if (radio.Checked)
                        {
                            return radio.Tag.ToString();
                        }
                    }
                }

                return null;
            }
        }

        private void cbStartup_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
