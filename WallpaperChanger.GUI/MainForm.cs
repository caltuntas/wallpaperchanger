﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Quartz;
using Quartz.Impl;
using WallpaperChanger.SmashingMagazine;

namespace WallpaperChanger.GUI
{
    

    public partial class MainForm : Form
    {
        private WallpaperParser parser = new WallpaperParser();
        private WallpaperCatalog currentCatalog;
        public MainForm()
        {
            InitializeComponent();
        }

 
        private void MainForm_Load(object sender, EventArgs e)
        {
            

            //sched.TriggerJob(JobKey.Create("job1","group1"));


            var bounds = Screen.PrimaryScreen.Bounds;
            

            var list = parser.GetWallpaperCatalogLinks();

            cmbMonths.DisplayMember = "Date";
            cmbMonths.DataSource = list;            
        }

        private void LoadImages(WallpaperCatalog catalog)
        {                    
            imageList.ImageSize = new Size(250, 156);
            foreach (var wallpaper in catalog.Wallpapers)
            {
                using (WebClient webClient = new WebClient())
                {
                    byte[] bitmapData;
                    bitmapData = webClient.DownloadData(wallpaper.PreviewImageUrl);
                    
                    MemoryStream memoryStream = new MemoryStream(bitmapData);
                    Bitmap bitmap = new Bitmap(memoryStream);
                    imageList.Images.Add(wallpaper.Name, bitmap);                   
                }
            }
        }

        private void cmbMonths_SelectedIndexChanged(object sender, EventArgs e)
        {
            lstImage.Clear();
            imageList.Images.Clear();

            var link = cmbMonths.SelectedItem as WallpaperCatalogLink;

            currentCatalog=parser.GetWallpaperCatalog(link);

            LoadImages(currentCatalog);            
            this.lstImage.View = View.LargeIcon;
            this.lstImage.LargeImageList = imageList;

            for (int j = 0; j < imageList.Images.Count; j++)
            {
                ListViewItem item = new ListViewItem();
                item.ImageIndex = j;
                this.lstImage.Items.Add(item);
            }
        }

        private void btnSetWallpaper_Click(object sender, EventArgs e)
        {
            var item =lstImage.SelectedItems[0] as ListViewItem;

            var imageKey =imageList.Images.Keys[item.ImageIndex];
            var wallpaper=currentCatalog.GetWallpaper(imageKey);
            var image =wallpaper.FindForResolution(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height,true);

            WallpaperSetter.Set(new Uri(image.Url), WallpaperSetter.Style.Stretched);
        }
    }
}
