﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Quartz;
using WallpaperChanger.SmashingMagazine;

namespace WallpaperChanger.GUI
{
    [DisallowConcurrentExecutionAttribute]
    [PersistJobDataAfterExecution]
    public class WallpaperChangeJob : IJob
    {
        private WallpaperParser parser;
        private WallpaperSettingsRepository repository;

        public WallpaperChangeJob(WallpaperParser parser,WallpaperSettingsRepository repository)
        {
            this.parser = parser;
            this.repository = repository;
        }       
        

        public void Execute(IJobExecutionContext context)
        {
            var settings = repository.Get();
            int imageIndex = settings.ImageIndex;

            var list = parser.GetWallpaperCatalogLinks();

            var link = list.GetLink(DateTime.Now);

            var currentCatalog = parser.GetWallpaperCatalog(link);
            var wallpaper = currentCatalog.Wallpapers[imageIndex];
            var image = wallpaper.FindForResolution(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height, false);

            WallpaperSetter.Set(new Uri(image.Url), WallpaperSetter.Style.Stretched);            
            
            if (imageIndex == currentCatalog.Wallpapers.Count - 1)
                settings.ImageIndex = 0;
            else
                settings.ImageIndex++;

            repository.Save(settings);
        }
    }
}
