﻿namespace WallpaperChanger.GUI
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gpInterval = new System.Windows.Forms.GroupBox();
            this.rb5Min = new System.Windows.Forms.RadioButton();
            this.btnSave = new System.Windows.Forms.Button();
            this.rb3Hours = new System.Windows.Forms.RadioButton();
            this.rbDay = new System.Windows.Forms.RadioButton();
            this.rbHour = new System.Windows.Forms.RadioButton();
            this.gpGeneral = new System.Windows.Forms.GroupBox();
            this.cbStartup = new System.Windows.Forms.CheckBox();
            this.gpInterval.SuspendLayout();
            this.gpGeneral.SuspendLayout();
            this.SuspendLayout();
            // 
            // gpInterval
            // 
            this.gpInterval.Controls.Add(this.rb5Min);
            this.gpInterval.Controls.Add(this.rb3Hours);
            this.gpInterval.Controls.Add(this.rbDay);
            this.gpInterval.Controls.Add(this.rbHour);
            this.gpInterval.Location = new System.Drawing.Point(12, 12);
            this.gpInterval.Name = "gpInterval";
            this.gpInterval.Size = new System.Drawing.Size(260, 130);
            this.gpInterval.TabIndex = 1;
            this.gpInterval.TabStop = false;
            this.gpInterval.Text = "Interval";
            // 
            // rb5Min
            // 
            this.rb5Min.AutoSize = true;
            this.rb5Min.Location = new System.Drawing.Point(7, 22);
            this.rb5Min.Name = "rb5Min";
            this.rb5Min.Size = new System.Drawing.Size(71, 17);
            this.rb5Min.TabIndex = 4;
            this.rb5Min.TabStop = true;
            this.rb5Min.Tag = "5";
            this.rb5Min.Text = "5 Minutes";
            this.rb5Min.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(170, 243);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // rb3Hours
            // 
            this.rb3Hours.AutoSize = true;
            this.rb3Hours.Location = new System.Drawing.Point(7, 68);
            this.rb3Hours.Name = "rb3Hours";
            this.rb3Hours.Size = new System.Drawing.Size(92, 17);
            this.rb3Hours.TabIndex = 2;
            this.rb3Hours.TabStop = true;
            this.rb3Hours.Tag = "180";
            this.rb3Hours.Text = "Every 3 Hours";
            this.rb3Hours.UseVisualStyleBackColor = true;
            // 
            // rbDay
            // 
            this.rbDay.AutoSize = true;
            this.rbDay.Location = new System.Drawing.Point(7, 91);
            this.rbDay.Name = "rbDay";
            this.rbDay.Size = new System.Drawing.Size(74, 17);
            this.rbDay.TabIndex = 1;
            this.rbDay.TabStop = true;
            this.rbDay.Tag = "1440";
            this.rbDay.Text = "Every Day";
            this.rbDay.UseVisualStyleBackColor = true;
            // 
            // rbHour
            // 
            this.rbHour.AutoSize = true;
            this.rbHour.Checked = true;
            this.rbHour.Location = new System.Drawing.Point(7, 45);
            this.rbHour.Name = "rbHour";
            this.rbHour.Size = new System.Drawing.Size(78, 17);
            this.rbHour.TabIndex = 0;
            this.rbHour.TabStop = true;
            this.rbHour.Tag = "60";
            this.rbHour.Text = "Every Hour";
            this.rbHour.UseVisualStyleBackColor = true;
            // 
            // gpGeneral
            // 
            this.gpGeneral.Controls.Add(this.cbStartup);
            this.gpGeneral.Location = new System.Drawing.Point(13, 149);
            this.gpGeneral.Name = "gpGeneral";
            this.gpGeneral.Size = new System.Drawing.Size(259, 74);
            this.gpGeneral.TabIndex = 4;
            this.gpGeneral.TabStop = false;
            this.gpGeneral.Text = "General";
            // 
            // cbStartup
            // 
            this.cbStartup.AutoSize = true;
            this.cbStartup.Location = new System.Drawing.Point(7, 20);
            this.cbStartup.Name = "cbStartup";
            this.cbStartup.Size = new System.Drawing.Size(149, 17);
            this.cbStartup.TabIndex = 0;
            this.cbStartup.Text = "Start when windows starts";
            this.cbStartup.UseVisualStyleBackColor = true;
            this.cbStartup.CheckedChanged += new System.EventHandler(this.cbStartup_CheckedChanged);
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(286, 284);
            this.Controls.Add(this.gpGeneral);
            this.Controls.Add(this.gpInterval);
            this.Controls.Add(this.btnSave);
            this.Name = "SettingsForm";
            this.Text = "Settings";
            this.gpInterval.ResumeLayout(false);
            this.gpInterval.PerformLayout();
            this.gpGeneral.ResumeLayout(false);
            this.gpGeneral.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gpInterval;
        private System.Windows.Forms.RadioButton rb3Hours;
        private System.Windows.Forms.RadioButton rbDay;
        private System.Windows.Forms.RadioButton rbHour;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.RadioButton rb5Min;
        private System.Windows.Forms.GroupBox gpGeneral;
        private System.Windows.Forms.CheckBox cbStartup;

    }
}