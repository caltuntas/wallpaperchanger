﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace WallpaperChanger.GUI
{
    public class WallpaperSettingsRepository
    {
        private const string SETTINGS_FILE = "WallpaperSettings.xml";
        public WallpaperSettings Get()
        {
            WallpaperSettings settings = null;
            FileInfo file = new FileInfo(SETTINGS_FILE);

            if (file.Exists)
            {
                var ser = new XmlSerializer(typeof(WallpaperSettings));
                using (var reader = XmlReader.Create(file.OpenText()))
                {
                    settings = (WallpaperSettings)ser.Deserialize(reader);                    
                }
            }
            else
            {
                settings = new WallpaperSettings();
                Save(settings);
            }

            return settings;
        }

        public void Save(WallpaperSettings settings)
        {
            FileInfo file = new FileInfo(SETTINGS_FILE);
            if (file.Exists)
                file.Delete();

            using (FileStream fs = new FileStream(SETTINGS_FILE, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite))
            {
                XmlSerializer s = new XmlSerializer(typeof(WallpaperSettings));
                s.Serialize(fs, settings);
            }
        }
    }
}
