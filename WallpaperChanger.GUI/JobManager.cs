﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quartz;
using Quartz.Spi;

namespace WallpaperChanger.GUI
{
    public class JobManager
    {
        private readonly IJobFactory _jobFactory;
        private readonly ISchedulerFactory _schedulerFactory;


        public JobManager(ISchedulerFactory schedulerFactory, IJobFactory jobFactory)
        {
            _schedulerFactory = schedulerFactory;
            _jobFactory = jobFactory;
        }

        public IScheduler Add<T>(ITrigger trigger) where T : IJob
        {
            IScheduler scheduler = _schedulerFactory.GetScheduler();
            scheduler.JobFactory = _jobFactory;
            scheduler.Start();
            IJobDetail jobDetail = JobBuilder
                .Create()
                .OfType(typeof(T))
                .WithIdentity(typeof(T).Name)                
                .Build();            
            scheduler.ScheduleJob(jobDetail, trigger);
            return scheduler;
        }
    }
}
