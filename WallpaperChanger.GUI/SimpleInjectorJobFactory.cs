﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using Quartz;
using Quartz.Spi;

namespace WallpaperChanger.GUI
{
    public class SimpleInjectorJobFactory : IJobFactory
    {
        private readonly Func<Type, IJob> jobFactory;

        public SimpleInjectorJobFactory(Func<Type, IJob> jobFactory)
        {
            this.jobFactory = jobFactory;
        }

        public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
        {
            IJobDetail jobDetail = bundle.JobDetail;
            Type jobType = jobDetail.JobType;
            var job = jobFactory(jobType);
            return job;           
        }

        public void ReturnJob(IJob job)
        {
            
        }
    }
}
