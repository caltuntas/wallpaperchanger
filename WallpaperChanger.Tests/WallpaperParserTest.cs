﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WallpaperChanger.SmashingMagazine;

namespace WallpaperChanger.Tests
{
    [TestClass]
    public class WallpaperParserTest
    {
        [TestMethod]
        public void GetWallpaperCatalogLinks()
        {
            WallpaperParser parser = new WallpaperParser();
            var list =parser.GetWallpaperCatalogLinks();
            Assert.IsNotNull(list);
            Assert.IsTrue(list.Links.Count>0);
        }

        [TestMethod]
        public void GetWallpaperCatalog()
        {
            WallpaperParser parser = new WallpaperParser();
            var list = parser.GetWallpaperCatalogLinks();
            var catalog =parser.GetWallpaperCatalog(list.Links[0]);
            Assert.IsNotNull(catalog);
            Assert.IsTrue(catalog.Wallpapers.Count > 0);
        }
    }
}
